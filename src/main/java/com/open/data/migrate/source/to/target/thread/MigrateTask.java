package com.open.data.migrate.source.to.target.thread;

import com.open.data.migrate.source.to.target.config.TableConfig;
import com.open.data.migrate.source.to.target.entity.ColumnInfo;
import com.open.data.migrate.source.to.target.entity.ColumnMapping;
import com.open.data.migrate.source.to.target.pool.ConnectionPool;
import com.open.data.migrate.source.to.target.service.impl.DaoServiceImpl;
import com.open.data.migrate.source.to.target.util.DbUtils;
import com.open.data.migrate.source.to.target.util.LogUtils;

import java.io.*;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author 2810010108@qq.com
 * @project open-data-migrate-source-to-target
 * @package com.open.data.migrate.source.to.target.thread
 * @date 2020/6/18 17:24
 **/
public class MigrateTask implements Runnable {

    private String sql;

    private TableConfig table;

    private long total;

    private CountDownLatch latch;


    private AtomicInteger counter;

    private long start;

    public MigrateTask(TableConfig table, long start, AtomicInteger counter, CountDownLatch latch, long total, String sql) {
        this.table = table;
        this.sql = sql;
        this.total = total;
        this.latch = latch;
        this.counter = counter;
        this.start = start;
    }


    @Override
    public void run() {
        // 查询出数据并且转码
        StringBuilder log = new StringBuilder();
        PreparedStatement ps = null;
        Connection connection = ConnectionPool.borrowTarget();
        long innerStart = System.currentTimeMillis();
        List<ColumnMapping> dataList = null;
        try {
            assert connection != null;
            connection.setAutoCommit(false);
            long start41 = System.currentTimeMillis();
            dataList = DaoServiceImpl.getInstance().queryList(table, total, counter, start, sql);
            long end41 = System.currentTimeMillis();
            int oldSize = dataList.size();
            long start42 = System.currentTimeMillis();
            filterAlreadyHandled(dataList);
            long end42 = System.currentTimeMillis();
            int size = dataList.size();
            if (size == 0) {
                return;
            }
            int stage = counter.addAndGet(size);
            // 首先过滤已经处理过的数据记录
            log.append(packLogNewLine("[第1/4阶段,查询数据]", stage, total, end41 - start41, end41 - start));

            log.append(packLogNewLine("[第2/4阶段,数据过滤" + (oldSize - size) + "条]", stage, total, end42 - start42, end42 - start));

            long start43 = System.currentTimeMillis();
            // 插入
            ColumnMapping first = dataList.get(0);
            List<ColumnInfo> firstRecord = first.getColumns();
            String colSql = "";
            String valSql = "";
            for (ColumnInfo col : firstRecord) {
                colSql += "," + col.getColumnName();
                valSql += ",?";
            }
            colSql = colSql.substring(1);
            valSql = valSql.substring(1);
            String insertSql = String.format("insert into %s(%s) values (%s) ", table.getTargetTable(), colSql, valSql);
            ps = connection.prepareStatement(insertSql);
            for (ColumnMapping mapping : dataList) {
                List<ColumnInfo> normalCols = mapping.getColumns();
                for (int i = 0; i < normalCols.size(); i++) {
                    ColumnInfo col = normalCols.get(i);
                    if (col.getValue() instanceof Blob) {
                        ps.setBinaryStream(i + 1, convertEncoding((Blob) col.getValue()));
                    } else {
                        ps.setObject(i + 1, col.getValue());
                    }
                }
                ps.addBatch();
            }
            ps.executeBatch();
            connection.commit();
            long end43 = System.currentTimeMillis();
            log.append(packLogNewLine("[第3/4阶段,数据插入目标库]", stage, total, end43 - start43, end43 - start));

            long start44 = System.currentTimeMillis();
            saveRecords(dataList, true);
            long end44 = System.currentTimeMillis();
            log.append(packLogNewLine("[第4/4阶段,保存记录]", stage, total, end44 - start44, end44 - start));
            log.append(packLogNewLine("[完成数据迁移]", stage, total, end44 - innerStart, end44 - start));
            String logContent = log.toString();
            LogUtils.printInfoLog(logContent);
        } catch (Exception e) {
            saveRecords(dataList, false);
            LogUtils.printErrorLog("迁移报错，错误信息：" + e.getMessage());
            e.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            ConnectionPool.returnTarget(connection);
            latch.countDown();
        }
    }

    private static String packLogNewLine(String stageName, int process, long total, long singleWaste, long totalWaste) {
        return String.format("当前线程:%s,字段编码转换%s,总体进度:%d/%d,单步耗时:%d(ms),总耗时:%d(ms)",
                Thread.currentThread().getName(),
                stageName,
                process, total, singleWaste, totalWaste) + "\n";
    }


    private synchronized void saveRecords(List<ColumnMapping> dataList, boolean success) {
        // 将成功的记录主键以行为单位记录到success.txt文本中
        BufferedWriter writer = null;
        try {
            String prefix = success ? "success-" : "failed-";
            File file = new File(prefix + table.getSourceTable() + ".txt");
            if (!file.exists()) {
                file.createNewFile();
            }
            writer = new BufferedWriter(new FileWriter(file, true));
            for (ColumnMapping mapping : dataList) {
                List<ColumnInfo> pkList = mapping.getPrimaryKey();
                String line = "";
                for (ColumnInfo pk : pkList) {
                    line += "," + pk.getColumnName() + "=" + pk.getValue();
                }
                line = line.substring(1);
                writer.write(line + "\n");
            }
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void filterAlreadyHandled(List<ColumnMapping> dataList) {
        File file = new File("success-" + table.getSourceTable() + ".txt");
        if (!file.exists()) {
            return;
        }
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            List<String> pkList = new ArrayList<String>();
            while (true) {
                String line = reader.readLine();
                if (line == null || "".equals(line.trim())) {
                    break;
                }
                pkList.add(line);
            }
            Iterator<ColumnMapping> iterator = dataList.iterator();
            while (iterator.hasNext()) {
                ColumnMapping mapping = iterator.next();
                List<ColumnInfo> pks = mapping.getPrimaryKey();
                String line = "";
                for (ColumnInfo pk : pks) {
                    line += "," + pk.getColumnName() + "=" + pk.getValue();
                }
                line = line.substring(1);
                if (pkList.contains(line)) {
                    // 成功的记录中已包含该id
                    // 删除掉
                    iterator.remove();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static InputStream convertEncoding(Blob source) throws UnsupportedEncodingException, SQLException {
        //blob 转 String
        String blobString = new String(source.getBytes(1, (int) source.length()), DbUtils.getSourceDbConfig().getEncoding());
        //String 转 blob
        Blob b = new com.mysql.cj.jdbc.Blob(blobString.getBytes(DbUtils.getTargetDbConfig().getEncoding()), null);
        return b.getBinaryStream();
    }
}
