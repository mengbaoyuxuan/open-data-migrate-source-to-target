package com.open.data.migrate.source.to.target.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author 2810010108@qq.com
 * @project open-component-migrate-to-mysql
 * @package com.open.component.convert.column.encoding.util
 * @date 2020/9/10 21:59
 **/
public class LogUtils {
    public static synchronized void printInfoLog(String logContent) {
        System.out.println(logContent);
        logInFile(logContent);
    }

    public static synchronized void printErrorLog(String logContent) {
        System.out.println(logContent);
        logInFile(logContent);
    }

    private static void logInFile(String logContent) {
        BufferedWriter writer = null;
        try {
            File file = new File("processing_log.txt");
            if (!file.exists()) {
                file.createNewFile();
            }
            writer = new BufferedWriter(new FileWriter(file, true));
            writer.write(logContent + "\n");
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
