package com.open.data.migrate.source.to.target.service;

import com.open.data.migrate.source.to.target.config.TableConfig;
import com.open.data.migrate.source.to.target.entity.ColumnMapping;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author 2810010108@qq.com
 * @project open-data-migrate-source-to-target
 * @package com.open.data.migrate.source.to.target.service
 * @date 2020/6/3 16:00
 **/
public interface DaoService {

    long count(String sql);

    /**
     * 查询列表数据
     *
     * @param sql
     * @return
     */
    List<ColumnMapping> queryList(TableConfig table, long total, AtomicInteger counter, long start, String sql);
}
