package com.open.data.migrate.source.to.target.config;

import lombok.Data;

/**
 * @author 2810010108@qq.com
 * @project open-data-migrate-source-to-target
 * @package com.open.data.migrate.source.to.target.config
 * @date 2020/6/2 17:47
 **/
@Data
public class DbConfig {
    private String driver;

    private String url;

    private String username;

    private String password;

    private String encoding;

    private int pageSize;

    private int threadNums;

    private int connections;

}
