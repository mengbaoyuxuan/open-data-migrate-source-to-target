package com.open.data.migrate.source.to.target.util;

import com.open.data.migrate.source.to.target.config.TableConfig;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;

/**
 * @author 2810010108@qq.com
 * @project open-data-migrate-source-to-target
 * @package com.open.data.migrate.source.to.target.util
 * @date 2020/6/3 10:29
 **/
public class ConfigUtils {
    private static final String tablePath = "tables.properties";

    public static List<TableConfig> loadTableConfig() {
        try {
            // 表明配置类似uspc_boss_file=download_link,
            // 表明配置类似bdc_formal_idcplatform=file_url,
            Properties prop = new Properties();
            File file = new File(tablePath);
            LogUtils.printInfoLog(String.format("从配置文件%s中读取需要迁移的表和字段配置", file.getAbsolutePath()));
            InputStream is = new FileInputStream(file);
            prop.load(is);
            List<TableConfig> tableConfigList = new ArrayList<TableConfig>();
            Enumeration keys = prop.propertyNames();
            while (keys.hasMoreElements()) {
                String key = (String) keys.nextElement();
                String tableNames = key.split(",")[0];
                String uniqueColumns = prop.getProperty(tableNames + ",uniqueColumns");
                String[] conditionCols = uniqueColumns.split("\\|");
                List<String> conditionColumns = new ArrayList<String>(Arrays.asList(conditionCols));
                TableConfig config = new TableConfig();
                config.setSourceTable(tableNames.split("->")[0]);
                config.setTargetTable(tableNames.split("->")[1]);
                config.setPrimaryKey(conditionColumns);
                if (!tableConfigList.contains(config)) {
                    tableConfigList.add(config);
                }
            }
            return tableConfigList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
