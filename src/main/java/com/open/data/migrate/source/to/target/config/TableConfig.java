package com.open.data.migrate.source.to.target.config;

import lombok.Data;

import java.util.List;
import java.util.Objects;

/**
 * @author 2810010108@qq.com
 * @project open-data-migrate-source-to-target
 * @package com.open.data.migrate.source.to.target.config
 * @date 2020/6/2 17:56
 **/
@Data
public class TableConfig {
    private String sourceTable;

    private String targetTable;

    private List<String> primaryKey;

    private String columnName;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TableConfig that = (TableConfig) o;
        return sourceTable.equals(that.sourceTable) &&
                targetTable.equals(that.targetTable) &&
                primaryKey.equals(that.primaryKey) &&
                columnName.equals(that.columnName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sourceTable, targetTable, primaryKey, columnName);
    }
}
