package com.open.data.migrate.source.to.target.enums;

import lombok.Getter;

/**
 * @author 2810010108@qq.com
 * @project open-component-migrate-to-mysql
 * @package com.open.component.convert.column.encoding.enums
 * @date 2020/9/10 16:10
 **/
@Getter
public enum SourceDbEnums implements DbEnum {
    /**
     * 源库为mysql库的key配置
     */
    SOURCE_MYSQL("jdbc.source.mysql.driverClassName", "jdbc.source.mysql.url", "jdbc.source.mysql.username", "jdbc.source.mysql.password", "jdbc.source.mysql.encoding"),
    /**
     * 源库为oracle库的key配置
     */
    SOURCE_ORACLE("jdbc.source.oracle.driverClassName", "jdbc.source.oracle.url", "jdbc.source.oracle.username", "jdbc.source.oracle.password", "jdbc.source.oracle.encoding");

    private String driver;

    private String url;

    private String username;

    private String password;

    private String encoding;

    SourceDbEnums(String driver, String url, String username, String password, String encoding) {
        this.driver = driver;
        this.url = url;
        this.username = username;
        this.password = password;
        this.encoding = encoding;
    }
}
