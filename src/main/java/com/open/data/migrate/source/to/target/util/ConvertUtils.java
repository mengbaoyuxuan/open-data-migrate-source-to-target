package com.open.data.migrate.source.to.target.util;

import com.open.data.migrate.source.to.target.config.TableConfig;
import com.open.data.migrate.source.to.target.pool.ConnectionPool;
import com.open.data.migrate.source.to.target.service.impl.DaoServiceImpl;
import com.open.data.migrate.source.to.target.thread.MigrateTask;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author 2810010108@qq.com
 * @project open-data-migrate-source-to-target
 * @package com.open.data.migrate.source.to.target.util
 * @date 2020/6/3 17:11
 **/
public class ConvertUtils {

    public static void exe() {
        // 读取表和字段配置
        long totalStart = System.currentTimeMillis();
        List<TableConfig> tableConfigList = ConfigUtils.loadTableConfig();
        ExecutorService executorService = Executors.newFixedThreadPool(DbUtils.getSourceDbConfig().getThreadNums());
        try {
            // 获取数据库连接
            assert tableConfigList != null;
            tableConfigList.parallelStream().forEach(table -> {
                try {
                    String countSql = String.format("select count(*) from %s", table.getSourceTable(), table.getColumnName());
                    long total = DaoServiceImpl.getInstance().count(countSql);
                    long validTotal = total - getSuccessLines(table.getSourceTable());
                    LogUtils.printInfoLog("过滤掉已转换的有效总数:" + total);
                    // 多线程查询
                    long pageSize = DbUtils.getSourceDbConfig().getPageSize();
                    int loopNums = (int) (total / pageSize) == 0 ? 1 : (int) (total / pageSize) + 1;
                    CountDownLatch latch = new CountDownLatch(loopNums);
                    AtomicInteger counter = new AtomicInteger();
                    long start = System.currentTimeMillis();
                    for (int pageNum = 1; pageNum <= loopNums; pageNum++) {
                        // 查询表table.tableName对应字段table.columnName

                        String pageSql = String.format("select t.* from (select ROWNUM AS rowno,tmp.* from %s tmp where ROWNUM<=%d) t WHERE t.rowno > %d",
                                table.getSourceTable(), pageSize * pageNum, pageSize * (pageNum - 1));
                        executorService.execute(new MigrateTask(table, start, counter, latch, validTotal, pageSql));
                    }
                    latch.await();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 关闭线程池
            LogUtils.printInfoLog("正在等待关闭线程池...");
            executorService.shutdown();
            LogUtils.printInfoLog("成功关闭线程池");
            // 关闭连接池
            LogUtils.printInfoLog("正在等待关闭连接池，释放数据库连接...");
            ConnectionPool.closeAllConnections();
            LogUtils.printInfoLog("成功释放所有连接");
            LogUtils.printInfoLog("恭喜你!迁移完成,整体耗时:" + (System.currentTimeMillis() - totalStart) + "ms");
        }
    }

    private static int getSuccessLines(String table) {
        File file = new File("success-" + table + ".txt");
        if (!file.exists()) {
            return 0;
        }
        BufferedReader reader = null;
        try {
            int lines = 0;
            reader = new BufferedReader(new FileReader(file));
            while (true) {
                String line = reader.readLine();
                if (line == null || "".equals(line.trim())) {
                    break;
                }
                lines++;
            }
            return lines;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return 0;
    }

}
