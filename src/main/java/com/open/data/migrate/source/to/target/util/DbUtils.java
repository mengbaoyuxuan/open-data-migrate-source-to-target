package com.open.data.migrate.source.to.target.util;

import com.open.data.migrate.source.to.target.config.DbConfig;
import com.open.data.migrate.source.to.target.enums.DbEnum;
import com.open.data.migrate.source.to.target.enums.SourceDbEnums;
import com.open.data.migrate.source.to.target.enums.TargetDbEnums;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author 2810010108@qq.com
 * @project open-data-migrate-source-to-target
 * @package com.open.data.migrate.source.to.target.util
 * @date 2020/6/2 17:54
 **/
public class DbUtils {

    private static final String dbFilePath = "jdbc.properties";

    private static class SingleHolder {
        private static DbConfig SOURCE_CONFIG = DbUtils.loadConfig(SourceDbEnums.values());
        private static DbConfig TARGET_CONFIG = DbUtils.loadConfig(TargetDbEnums.values());
    }


    public static DbConfig getSourceDbConfig() {
        return SingleHolder.SOURCE_CONFIG;
    }

    public static DbConfig getTargetDbConfig() {
        return SingleHolder.TARGET_CONFIG;
    }

    private static DbConfig loadConfig(DbEnum[] dbEnums) {
        try {
            Properties prop = new Properties();
            File file = new File(dbFilePath);
            InputStream is = new FileInputStream(file);
            prop.load(is);
            LogUtils.printInfoLog(String.format("从配置文件%s中读取源库配置信息", file.getAbsolutePath()));
            DbConfig config = new DbConfig();
            for (DbEnum enums : dbEnums) {
                String driver = prop.getProperty(enums.getDriver());
                if (driver != null) {
                    config.setDriver(driver);
                    config.setUrl(prop.getProperty(enums.getUrl()));
                    config.setUsername(prop.getProperty(enums.getUsername()));
                    config.setPassword(prop.getProperty(enums.getPassword()));
                    config.setEncoding(prop.getProperty(enums.getEncoding()));
                    config.setPageSize(Integer.parseInt(prop.getProperty("column.handle.page.size")));
                    config.setThreadNums(Integer.parseInt(prop.getProperty("column.handle.concurrent.threads")));
                    config.setConnections(Integer.parseInt(prop.getProperty("column.handle.concurrent.connections")));
                    break;
                }
            }
            return config;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}
