package com.open.data.migrate.source.to.target.enums;

/**
 * @author 2810010108@qq.com
 * @project open-component-migrate-to-mysql
 * @package com.open.component.convert.column.encoding.enums
 * @date 2020/9/10 17:17
 **/
public interface DbEnum{
    public String getDriver();

    public String getUrl();

    public String getUsername();

    public String getPassword();

    public String getEncoding();
}
