package com.open.data.migrate.source.to.target.enums;

import lombok.Getter;

/**
 * @author 2810010108@qq.com
 * @project open-component-migrate-to-mysql
 * @package com.open.component.convert.column.encoding.enums
 * @date 2020/9/10 16:10
 **/
@Getter
public enum TargetDbEnums implements DbEnum {
    /**
     * 目标库为mysql库的key配置
     */
    TARGET_MYSQL("jdbc.target.mysql.driverClassName", "jdbc.target.mysql.url", "jdbc.target.mysql.username", "jdbc.target.mysql.password", "jdbc.target.mysql.encoding"),
    /**
     * 目标库为oracle库的key配置
     */
    TARGET_ORACLE("jdbc.target.oracle.driverClassName", "jdbc.target.oracle.url", "jdbc.target.oracle.username", "jdbc.target.oracle.password", "jdbc.target.oracle.encoding");

    private String driver;

    private String url;

    private String username;

    private String password;

    private String encoding;

    TargetDbEnums(String driver, String url, String username, String password, String encoding) {
        this.driver = driver;
        this.url = url;
        this.username = username;
        this.password = password;
        this.encoding = encoding;
    }

}
