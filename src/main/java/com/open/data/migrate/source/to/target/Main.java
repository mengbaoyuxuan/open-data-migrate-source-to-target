package com.open.data.migrate.source.to.target;

import com.open.data.migrate.source.to.target.util.ConvertUtils;

/**
 * @author 2810010108@qq.com
 * @project open-data-migrate-source-to-target
 * @package com.open.data.migrate.source.to.target.config
 * @date 2020/6/2 17:51
 **/
public class Main {
    public static void main(String[] args) {
        ConvertUtils.exe();
    }
}
