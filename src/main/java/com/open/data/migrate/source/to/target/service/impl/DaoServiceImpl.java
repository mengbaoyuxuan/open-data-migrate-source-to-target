package com.open.data.migrate.source.to.target.service.impl;

import com.open.data.migrate.source.to.target.config.TableConfig;
import com.open.data.migrate.source.to.target.entity.ColumnInfo;
import com.open.data.migrate.source.to.target.entity.ColumnMapping;
import com.open.data.migrate.source.to.target.pool.ConnectionPool;
import com.open.data.migrate.source.to.target.service.DaoService;
import com.open.data.migrate.source.to.target.util.LogUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author 2810010108@qq.com
 * @project open-data-migrate-source-to-target
 * @package com.open.data.migrate.source.to.target.service.impl
 * @date 2020/6/3 16:20
 **/
public class DaoServiceImpl implements DaoService {
    private static class SingleHolder {
        private static DaoServiceImpl instance = new DaoServiceImpl();
    }

    public static DaoService getInstance() {
        return DaoServiceImpl.SingleHolder.instance;
    }


    @Override
    public long count(String sql) {
        Connection connection = ConnectionPool.borrowSource();
        try {
            assert connection != null;
            PreparedStatement ps = connection.prepareStatement(sql);
            LogUtils.printInfoLog("查询数据总数开始,sql:" + sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                long total = rs.getLong(1);
                LogUtils.printInfoLog("查询数据总数完成,sql:" + sql + " 查询总数:" + total);
                return total;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionPool.returnSource(connection);
        }
        return 0L;
    }

    @Override
    public List<ColumnMapping> queryList(TableConfig table, long total, AtomicInteger counter, long start, String sql) {

        List<ColumnMapping> maps = new ArrayList<ColumnMapping>();
        Connection connection = ConnectionPool.borrowSource();
        try {
            assert connection != null;
            connection.setAutoCommit(false);
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (true) {
                if (!rs.next()) {
                    break;
                }
                ResultSetMetaData metaData = rs.getMetaData();
                ColumnMapping kv = new ColumnMapping();
                List<ColumnInfo> primaryKeyList = new ArrayList<ColumnInfo>();

                List<String> primaryKeys = table.getPrimaryKey();
                List<ColumnInfo> normalColumns = new ArrayList<ColumnInfo>();
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    String columnName = metaData.getColumnName(i);
                    for (int j = 0; j < primaryKeys.size(); j++) {
                        if (columnName.equals(primaryKeys.get(j))) {
                            // 字段相同，说明是主键,主键用字符串表示
                            ColumnInfo pkColumn = new ColumnInfo();
                            pkColumn.setColumnName(columnName);
                            pkColumn.setValue(rs.getString(i));
                            primaryKeyList.add(pkColumn);
                        }
                    }
                    if (!"ROWNO".equals(columnName.toUpperCase())) {
                        ColumnInfo normalCol = new ColumnInfo();
                        normalCol.setColumnName(columnName);
                        normalCol.setValue(rs.getObject(i));
                        normalColumns.add(normalCol);
                    }
                }
                kv.setTableName(table.getSourceTable());
                kv.setPrimaryKey(primaryKeyList);
                kv.setColumns(normalColumns);
                maps.add(kv);
            }
            return maps;
        } catch (Exception e) {
            LogUtils.printErrorLog("出错的sql：" + sql);
            e.printStackTrace();
        } finally {
            ConnectionPool.returnSource(connection);
        }
        return maps;
    }
}
