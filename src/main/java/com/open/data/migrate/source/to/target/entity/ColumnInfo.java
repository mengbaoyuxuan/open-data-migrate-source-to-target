package com.open.data.migrate.source.to.target.entity;

import lombok.Data;

/**
 * @author 2810010108@qq.com
 * @project open-data-migrate-source-to-target
 * @package com.open.component.convert.column.encoding.entity
 * @date 2020/9/7 14:16
 **/
@Data
public class ColumnInfo {

    private String columnName;

    private Object value;
}
