## 功能说明：
- 本jar实现的功能：从源库读取数据迁移到目标库，无任何框架，纯jdbc方式，数据流向支持mysql到mysql，mysql到oracle，oracle到oracle，oracle到mysql
    >读取table.properties配置中的表的配置,将配置的表的数据从源库迁移到目标库，
    >该工具适用于无法通过sqlinesdata迁移的场景。


- 注意：
    
    （1）本jar包需要使用jdk1.8运行
- 特别注意：
    
    （1）编码格式请谨慎配置。

## 使用说明：
1. lib目录
>jar包运行必须依赖的库文件
2. run.cmd
>jar运行脚本
3. jdbc.properties
>数据库连接配置信息，见配置文件内说明
4. tables.properties
    - 示例格式：SMS_SEND->SMS_SEND_COPY,uniqueColumns=SMS_ID
    - 说明：
        - （1）SMS_SEND：代表需要迁移的表名；
        - （2）->：固定，表示数据流向，用于分隔源表和目标表；
        - （3）SMS_SEND_COPY：代表数据迁移到目标库的表名；
        - （4）,：半角逗号，用于分隔表和字段的配置
        - （5）uniqueColumns：固定，代表该表唯一列，可以是主键，可以是其他唯一的条件，用于过滤已经迁移过的数据。
        - （6）=：固定；
        - （7）SMS_ID：代表唯一列的字段名称，必须，多个以竖线|分隔


## 使用步骤：
1. 修改jdbc.properties，配置正确的源库和目标库连接信息
2. 配置tables.properties，配置上需要迁移的表名和唯一列名
3. 双击run.cmd运行（Linux下运行./run.sh）
4. 查看生成的success打头的txt文件，存储的是迁移成功的记录，failed打头的txt文件，存储的是迁移失败的记录。
5. 迁移过程的数据可以通过控制台查看日志，也可以查看当前目录生成的processing_log.txt文件中存储的运行日志


## 技术交流
有兴趣的可以加入交流群，交流学习
QQ群号：566654343
